package app.somacode.shooter

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.ScreenUtils

class Game : ApplicationAdapter() {

    private lateinit var _batch: SpriteBatch
    private lateinit var _img: Texture

    override fun create() {
        _batch = SpriteBatch()
        _img = Texture("badlogic.jpg")
    }

    override fun render() {
        ScreenUtils.clear(1f, 0f, 0f, 1f)
        _batch.begin()
        _batch.draw(_img, 0f, 0f)
        _batch.end()
    }

    override fun dispose() {
        _batch.dispose()
        _img.dispose()
    }
}
